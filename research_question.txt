Group: group 187

Question
========

RQ: Is there a corelation between the Aroma and Sweetness of Coffee ?

Null Hypothesis: There is no corelation between the Aroma and Sweetness of Coffee.

Alternate Hypothesis: There is corelation between the Aroma and Sweetness of a Coffee.

Dataset
========

URL: https://www.kaggle.com/volpatto/coffee-quality-database-from-cqi?select=arabica_data_cleaned.csv

Column Headings:

> colnames(arabica_data_cleaned)
 [1] "V1"  "V2"  "V3"  "V4"  "V5"  "V6"  "V7"  "V8"  "V9"  "V10" "V11"
[12] "V12" "V13" "V14" "V15" "V16" "V17" "V18" "V19" "V20" "V21" "V22"
[23] "V23" "V24" "V25" "V26" "V27" "V28" "V29" "V30" "V31" "V32" "V33"
[34] "V34" "V35" "V36" "V37" "V38" "V39" "V40" "V41" "V42" "V43" "V44"